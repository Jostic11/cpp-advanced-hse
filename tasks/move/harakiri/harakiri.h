#pragma once

#include <string>

// Should not allow reuse and yell under sanitizers.
// Fix the interface and implementation.
// AwesomeCallback should add "awesomeness".

class OneTimeCallback {
public:
    virtual ~OneTimeCallback() = default;
    virtual std::string operator()() & = delete;
    virtual std::string operator()() const && = 0;
};

// Implement ctor, operator(), maybe something else...
class AwesomeCallback : public OneTimeCallback {
public:
    AwesomeCallback(std::string s) : s_(new std::string(s + "awesomeness")) {}

    std::string operator()() const && override {
        std::string tmp = *s_;
        delete this;
        return tmp;
    }

    ~AwesomeCallback() {
        if (s_) {
            delete s_;
        }
    }

private:
    std::string *s_;
};
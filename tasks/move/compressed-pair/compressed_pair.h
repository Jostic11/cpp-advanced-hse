#pragma once

#include <type_traits>
#include <stdio.h>
#include <utility>

template <typename T, size_t S, bool = std::is_empty_v<T> && !std::is_final_v<T>>
class CompressedPairElement;

template <typename T, size_t S>
class CompressedPairElement<T, S, false> {
public:
    CompressedPairElement() : value_() {}
    CompressedPairElement(const T& value) : value_(value) {}
    CompressedPairElement(T&& value) : value_(std::forward<T>(value)) {}

    T& Get() {
        return value_;
    }

    const T& Get() const {
        return value_;
    }

private:
    T value_;
};

template <typename T, size_t S>
class CompressedPairElement<T, S, true> : T {
public:
    CompressedPairElement() : T() {}
    CompressedPairElement(const T& element) : T(element) {}
    CompressedPairElement(T&& element) : T(std::forward<T>(element)) {}

    T& Get() {
        return *this;
    }

    const T& Get() const {
        return *this;
    }
};

template <typename F, typename S>
class CompressedPair : CompressedPairElement<F, 0>, CompressedPairElement<S, 1> {
public:
    CompressedPair() : CompressedPairElement<F, 0>()
                     , CompressedPairElement<S, 1>() {}
    CompressedPair(const F& first, const S& second) : CompressedPairElement<F, 0>(first)
                                                    , CompressedPairElement<S, 1>(second) {}
    CompressedPair(const F& first, S&& second) : CompressedPairElement<F, 0>(first)
                                               , CompressedPairElement<S, 1>(std::forward<S>(second)) {}
    CompressedPair(F&& first, S&& second) : CompressedPairElement<F, 0>(std::forward<F>(first))
                                          , CompressedPairElement<S, 1>(std::forward<S>(second)) {}

    F& GetFirst() {
        return CompressedPairElement<F, 0>::Get();
    }

    const F& GetFirst() const {
        return CompressedPairElement<F, 0>::Get();
    }

    S& GetSecond() {
        return CompressedPairElement<S, 1>::Get();
    };

    const S& GetSecond() const {
        return CompressedPairElement<S, 1>::Get();
    };
};